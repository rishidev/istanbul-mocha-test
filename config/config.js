var statusMessage200 = "status is 200"

module.exports = function () {
	return {
		usingWebNow: true,

		webConfig: {
					protocol: "https",
					host: "jsonplaceholder.typicode.com",
				},
		localhostConfig: {
					protocol: "http",
					host: "127.0.0.1",
					port: "8080"
				},

		endpoints: {
			"GET": {

				/*********************** CONFIG FOR TESTING FAKE APIS ****************************************/
				//route, message related to REST API response

				/*	POSTS */
				"POSTS": [
					["/posts", "GETTING LIST OF ALL POSTS - status is 200"],
					["/posts/1", "GETTING A POST WITH ID 1 - status is 200"],
				],
				/* COMMENTS */
				"COMMENTS": [
					["/comments", "GETTING LIST OF ALL COMMENTS - status is 200"],
					["/comments/5", "GETTING A COMMENT WITH ID 5 - status is 200"],
				],

				/* ALBUMS */
				"ALBUMS": [
					["/albums", "GETTING LIST OF ALL ALBUMS - status is 200"],
					["/albums/4", "GETTING A ALBUM WITH ID 4 - status is 200"]
				],

				/* PHOTOS */
				"PHOTOS": [
					["/photos", "GETTING LIST OF ALL PHOTOS - status is 200"],
					["/photos/7", "GETTING A PHOTO WITH ID 7 - status is 200"]
				],

				/* TODOS */
				"TODOS": [
					["/todos", "GETTING LIST OF ALL TODOS - status is 200"],
					["/todos/9", "GETTING A TODO WITH ID 9 - status is 200"]
				],

				/* USERS */
				"USERS": [
					["/users", "GETTING LIST OF ALL USERS - status is 200"],
					["/users/10", "GETTING A USER WITH ID 10 - status is 200"]
				],

				/*************** CONFIG FOR TESTING GENESIS APIS ********************************/

				"catalogService - Test": [
					["/catalog", "/catalog - " + statusMessage200],
					["/catalog/addpage/metadata", "/catalog/addpage/metadata - " + statusMessage200],
				],

				"commonService - Test": [
					["/countries", "/countries - " + statusMessage200],
					["/profiles", "/profiles - " + statusMessage200]
				],

				"contractService - Test" : [
					["/contract", "/contract - " + statusMessage200],

					/* /contract/:contractId */
					["/contract/2", "/contract/2 - " + statusMessage200],

					/* /contract/order/:orderProductId */
					["/contract/order/5", "/contract/order/5 - " + statusMessage200],

					/* /contract/generateInvoice/:fromDate */
					["/contract/generateInvoice/20-05-2017", "/contract/generateInvoice/20-05-2017 - " + statusMessage200]
				],

				"contractsBillingService" : [

				],

				"couponService - Test": [
					["/coupon", "/coupon - " + statusMessage200],

					/* /coupon/:couponId */
					["/coupon/7", "/coupon/7 - " + statusMessage200],

					["/coupon/metadata", "/coupon/metadata - " + statusMessage200],
				]
			},

			"POST": {

				"Authentication" : [
					["/api/v1/auth/signIn", "/api/v1/auth/signIn - signIn",401, {password: "Pass@23", username: "rishikesh.agrawani@relevancelab.co", captcha: "124"}, {"message":"Missing credentials"}],
					["/api/v1/auth/signIn", "/api/v1/auth/signIn - signIn", 400, {password: "Pass@123", username: "rishikesh.agrawani@relevancelab.com", captcha: "48036"}, {"message":"Missing credentials"}],
					["/api/v1/auth/signIn", "/api/v1/auth/signIn - signIn",200, {password: "Pass@123", username: "kislayapant@gmail.com", captcha: "48036"}, {"message":"Missing credentials"}]
				]
			}
		}
	}
}