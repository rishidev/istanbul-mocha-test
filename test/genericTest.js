/*
	{
		"date_of_creation": "27 Sept. 2017",
		"aim": "Rest Api Unit Testing"
	}
*/ 

var should = require("should");
var request = require("request");
var supertest = require('supertest');

var server = supertest.agent("http://127.0.0.1:3000");

var expect = require("chai").expect;
var assert = require("chai").assert;

var funcThatGivesObject = require("../config/config.js")
var config = funcThatGivesObject()

var baseUrl2 = "http://127.0.0.1:3000"

var postTest = function (item) {
    it(item[1], function(done) {
        supertest("http://127.0.0.1:3000")
            .post(item[0])
            .send(item[3])
            .expect(item[2])
            .expect(function(res) {
              console.log(res)
                // assert.equal(res.body.code, item[]);
                assert.equal(200, 200);
            })
            .end();
        done()
    });
}

function genericTester(){
	
	var baseUrl = ""

	if(config.usingWebNow) {
		baseUrl += config.webConfig.protocol + "://" + config.webConfig.host
	} else {
		baseUrl += config.localhostConfig.protocol + "://" + config.localhostConfig.host + ":" +config.localhostConfig.port
	}

	describe("TESTING RESTFUL APIS:-", function(){
		for(method in config.endpoints) {
			if(method == "GET"){
				let configGetObj = config.endpoints[method]

				for(let describeMessage in configGetObj){
					describe(describeMessage + " | " + method, function(){
						let configArr = configGetObj[describeMessage]
						for(let arr of configArr){
							let endpoint = arr[0] //endpoint
							let message = arr[1] //message for it

							it(message, function(done){
								//Making REST API call(GET) to fetch posts
						 		request.get({url: baseUrl + endpoint }, function(error, response, body){
						 			expect(response.statusCode).to.equal(200);	
						 		})

						 		done();
							})
						}
					})
				}
			} else {
				if(method == "POST") {		
					let configGetObj = config.endpoints[method]

					console.log(configGetObj)

					for(let describeMessage of configGetObj["Authentication"]){
						console.log(describeMessage)
						postTest(describeMessage)
					}
					// for(let describeMessage in configGetObj){	
					// 	describe(describeMessage + " | " + method, function (){
					// 		let configArr = configGetObj[describeMessage]
					// 			for(let arr of configArr){
					// 				let endpoint = arr[0] //endpoint
					// 				let message = arr[1] //message for it
					// 				let status = arr[2] //status 
					// 				let postObject = arr[3] //Post data to be sent
					// 				let expectedOutput = JSON.stringify(arr[4]) //expected 

					// 				console.log(endpoint, message, status, postObject, expectedOutput)
					// 				it(message, function(done){
					// 					// console.log("TRIAL")
					// 				    // console.log(done)
					// 				    try{
									    	
									    	
					// 				    	server
					// 					      .post('/api/v1/auth/signIn')
					// 					      .send({username: "rishikesh.agrawani@relevancelab.com", password: "Pass@123", captcha: "1234"})
					// 					      .expect("Content-type",/json/)
					// 					      .expect(200)
					// 					      .end(function(err,res){
					// 						      	console.log("GENESIS: ", res)
					// 						        res.status.should.equal(200);
					// 						        // res.body.error.should.equal(false);
					// 						        // res.body.data.should.equal(40);
					// 						        console.log(res.body)										        
					// 					      });
					// 					} catch(err){
					// 						console.log("Error")
					// 						console.log(err)
					// 					}
					// 					      // .expect(expectedOutput, done);
					// 					done()
					// 					server = supertest.agent("http://127.0.0.1:3000");
					// 				});
					// 			}
					// 	})
					// }



				}
			}
		}
})
}

genericTester()
